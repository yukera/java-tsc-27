package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IProjectService extends IService<Project> {

    void clear(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator);

    @Nullable
    Project add(@Nullable String userId,@Nullable Project project);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Optional<Project> findById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<Project> findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<Project> findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Optional<Project> remove(@Nullable String userId,@Nullable Project project);

    @NotNull
    Optional<Project> removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<Project> removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<Project> removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project startProjectById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project startProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project startProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project finishProjectById(@Nullable String userId, @Nullable String id);

    @NotNull
    Project finishProjectByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Project finishProjectByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
