package com.tsc.jarinchekhina.tm.dto;

import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    @Nullable
    private List<Project> projects;

    @Nullable
    private List<Task> tasks;

    @Nullable
    private List<User> users;

}
