package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected Predicate<E> predicateById(@NotNull final String id) {
        return e -> id.equals(e.getId());
    }

    @NotNull
    protected List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void addAll(@NotNull final Collection<E> collection) {
        collection.forEach(this::add);
    }

    @NotNull
    @Override
    public Optional<E> findById(@NotNull final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public Optional<E> removeById(@NotNull final String id) {
        @NotNull final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @NotNull
    @Override
    public Optional<E> remove(@NotNull final E entity) {
        entities.remove(entity);
        return Optional.of(entity);
    }

}
