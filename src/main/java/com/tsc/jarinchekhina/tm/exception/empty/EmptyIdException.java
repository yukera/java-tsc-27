package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
