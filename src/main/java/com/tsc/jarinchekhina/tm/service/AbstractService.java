package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        repository.addAll(collection);
    }

    @NotNull
    @Override
    public Optional<E> findById(@NotNull final String id) {
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public Optional<E> removeById(@NotNull final String id) {
        return repository.removeById(id);
    }

    @NotNull
    @Override
    public Optional<E> remove(@NotNull final E entity) {
        return repository.remove(entity);
    }

}
